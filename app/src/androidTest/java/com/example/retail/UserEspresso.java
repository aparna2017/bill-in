package com.example.retail;

/**
 * Created by haparn on 11/3/2017.
 */

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.retail.android2.R;
import com.example.retail.android2.UserActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.StringStartsWith.startsWith;

@RunWith(AndroidJUnit4.class)
public class UserEspresso {

    @Rule
    public ActivityTestRule<UserActivity> mActivityRule =
            new ActivityTestRule<>(UserActivity.class);


    @Test
    public void clickEditButton() {
        // Validation of edit save functionality
        // onView(withId(R.id.name)).perform(typeText("Mark 456")); // as expected got an error at this line as the field is disabled at this stage
        onView(withId(R.id.accIcon)).perform(click());
        onView(withId(R.id.name)).perform(typeText("Mark 456"));
        onView(withId(R.id.name)).perform(closeSoftKeyboard());
        onView(withId(R.id.phone)).perform(typeText("12345678")); // will work without "closeSoftKeyboard' too.
        onView(withId(R.id.accIcon)).perform(click());
        onView(withText(startsWith("Data saved successfully"))).inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
    }

    @Test
    public void clickLogoutButton() {
        // Press the button and check if it navigates to Login page
        onView(withId(R.id.logout)).perform(click());

        // This view is in a different Activity, no need to tell Espresso.
        onView(withId(R.id.accTitle));
    }

}
