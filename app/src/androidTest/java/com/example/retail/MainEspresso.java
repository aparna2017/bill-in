package com.example.retail;

/**
 * Created by haparn on 10/31/2017.
 */

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.retail.android2.MainActivity;
import com.example.retail.android2.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;

import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.StringStartsWith.startsWith;

@RunWith(AndroidJUnit4.class)
public class MainEspresso {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);


    @Test
    public void checkLoadedData() {
        // Check if item list is loaded and displayed
        onData(withText("Vaseline"));
       // onData(withId(R.id.scrollViewMain)).check(matches(isDisplayed())); // error ???????
    }


    @Test
    public void clickPaymentButton() {
        // Press the button and check if it navigates to PG page
        onView(withId(R.id.frameLayout)).check(matches(isCompletelyDisplayed()));
        onView(withId(R.id.button_proceed)).perform(click());

        // This view is in a different Activity, no need to tell Espresso.
        onView(withId(R.id.pgsampletext));
    }

    @Test
    public void clickScanButton() {
        // Press the button and check if toast is displayed
        onView(withId(R.id.frameLayout)).check(matches(isCompletelyDisplayed()));
        onView(withId(R.id.button_scan)).perform(click());
        onView(withText(startsWith("This application "))).inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));

    }
}
