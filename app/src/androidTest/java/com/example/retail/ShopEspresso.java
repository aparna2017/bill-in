package com.example.retail;

/**
 * Created by haparn on 11/3/2017.
 */

import android.support.test.espresso.ViewAssertion;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.retail.android2.MainActivity;
import com.example.retail.android2.PaymentActivity;
import com.example.retail.android2.R;
import com.example.retail.android2.ShopActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;

import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.hasContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isSelected;
import static android.support.test.espresso.matcher.ViewMatchers.withChild;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.StringStartsWith.startsWith;

@RunWith(AndroidJUnit4.class)
public class ShopEspresso {

    @Rule
    public ActivityTestRule<ShopActivity> mActivityRule =
            new ActivityTestRule<>(ShopActivity.class);

    @Test
    public void clickSettingsButton() {
        // Press the button and check if it navigates to Settings page of the app.
        onView(withId(R.id.cardview2)).check(matches(isCompletelyDisplayed()));
        onView(withId(R.id.settingsbtn)).perform(click());

        // This view is in a different Activity, no need to tell Espresso.
        onView(withText("FEEDBACK"));
    }

    @Test
    public void clickOffersButton() {
        // Press the button and check if it navigates to Offers page
        onView(withId(R.id.cardview2)).check(matches(isCompletelyDisplayed()));
        onView(withId(R.id.offersbtn)).perform(click());

        // This view is in a different Activity, no need to tell Espresso.
        onView(withText("OFFERS"));
    }

    @Test
    public void clickUserButton() {
        // Press the button and check if it navigates to User's profile page
        onView(withId(R.id.cardview2)).check(matches(isCompletelyDisplayed()));
        onView(withId(R.id.userbtn)).perform(click());

        // This view is in a different Activity, no need to tell Espresso.
        onView(withText("LOGOUT"));
    }

    @Test
    public void clickStore() {
        // Press the button and check if it navigates to Store page for scanning and adding products
        onView(withId(R.id.cardview2)).check(matches(isCompletelyDisplayed()));
        onView(withId(R.id.cardview1)).perform(click());

        // This view is in a different Activity, no need to tell Espresso.
        onView(withId(R.id.frameLayout));
    }



}
