package com.example.retail.android2;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by haparn on 6/21/2017.
 */
public class MoreActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ListView listOptions1 = getListView();
        List<String> optionArray = new ArrayList<String>();
        optionArray.add("\nABOUT US \n\n"+"We aim to improve your shopping experience by reducing your in-store wait time\n");
        optionArray.add("\nCONTACT US \n\n"+"Please call us on 12345678 on write in to car@billin.com\n");
        optionArray.add("\nCHAT WITH US \n\n"+"Rate us on playstore and tell us what you would like to improve\n");
        optionArray.add("\nFEEDBACK \n\n"+"Rate us on playstore and tell us what you would like to improve\n");

        ArrayAdapter optionsAdapter1 = new ArrayAdapter(this,android.R.layout.simple_list_item_1,optionArray);
        listOptions1.setAdapter(optionsAdapter1);
        listOptions1.setBackgroundResource(R.color.darkgrey);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
