package com.example.retail.android2;

import android.app.Activity;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by haparn on 6/21/2017.
 */
public class UserActivity extends Activity {

    private SQLiteDatabase db;
    private Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sp2 = getSharedPreferences("Login", MODE_PRIVATE);
        String mob = sp2.getString("mob", null);
        String name = sp2.getString("name", null);
        String email = sp2.getString("mail", null);
        String pwd = sp2.getString("pwd", null);

        setContentView(R.layout.activity_user);

        // Setting data to PI fields.
        TextView tviewName = (TextView) findViewById(R.id.name); tviewName.setText(name);
        TextView tviewEmail = (TextView) findViewById(R.id.email); tviewEmail.setText(email);
        TextView tviewPhone = (TextView) findViewById(R.id.phone); tviewPhone.setText(mob);


    }

    // User's PI data will be stored to table and also in Shared Preferences.
    public void editClicked(View view){
        ImageButton imgBtn = (ImageButton) findViewById(R.id.accIcon);
        TextView tviewName = (TextView) findViewById(R.id.name);
        TextView tviewEmail = (TextView) findViewById(R.id.email);
        TextView tviewPhone = (TextView) findViewById(R.id.phone);
        if(tviewName.isEnabled()==true){
            imgBtn.setImageResource(R.drawable.edit32);
            tviewName.setEnabled(false);
            tviewEmail.setEnabled(false);
            tviewPhone.setEnabled(false);

            try{
                SQLiteOpenHelper raagaDatabaseHelper = new AppDatabaseHelper(UserActivity.this);
                db = raagaDatabaseHelper.getWritableDatabase();
                ContentValues userValues = new ContentValues();
                userValues.put("FIRST_NAME",tviewName.getText().toString());
                userValues.put("EMAIL",tviewEmail.getText().toString());
                userValues.put("MOBILE",Integer.valueOf(tviewPhone.getText().toString()));
                db.insert("USER",null,userValues);
            }catch(SQLiteException e){
                Toast toast = Toast.makeText(UserActivity.this, "Database unavailable" ,Toast.LENGTH_LONG);
                toast.show();
                return;
            }

            SharedPreferences sp2 = getSharedPreferences("Login", MODE_PRIVATE);
            SharedPreferences.Editor Ed = sp2.edit();
            Ed.putString("name", tviewName.getText().toString());
            Ed.putString("mob", tviewPhone.getText().toString());
            Ed.putString("mail", tviewEmail.getText().toString());
            Toast toast = Toast.makeText(UserActivity.this, "Data saved successfully" ,Toast.LENGTH_LONG);
            toast.show();
            return;
        }
        if(tviewName.isEnabled()==false){
            imgBtn.setImageResource(R.drawable.save32);
            tviewName.setEnabled(true);
            tviewEmail.setEnabled(true);
            tviewPhone.setEnabled(true);
            return;
        }


    }

    public void logout(View view){
        SharedPreferences sp2 = getSharedPreferences("Login", MODE_PRIVATE);
        SharedPreferences.Editor Ed = sp2.edit();
        Ed.putString("mob", null);
        Ed.putString("pwd", null);
        Ed.putString("name", null);
        Ed.putString("mail", null);
        Ed.commit();

        Intent loggedout = new Intent(UserActivity.this, LoginActivity.class);
        startActivity(loggedout);
    }

    public void resetPwd(View view){};

    public void orderHistory(View view){};

   @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
