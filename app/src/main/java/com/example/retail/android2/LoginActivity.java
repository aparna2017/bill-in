package com.example.retail.android2;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.retail.zxing.IntentIntegrator;
import com.example.retail.zxing.IntentResult;

import java.security.MessageDigest;
import java.util.ArrayList;

import static com.example.retail.android2.CryptWithMD5.cryptWithMD5;
import static java.security.spec.MGF1ParameterSpec.SHA1;

// import android.support.v7.app.AppCompatActivity;


public class LoginActivity extends Activity {


    private TableLayout table;
    private SQLiteDatabase db;
    private Cursor cursor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sp1 = getSharedPreferences("Login", MODE_PRIVATE);
        sp1= getSharedPreferences("Login", MODE_PRIVATE);
        String mob = sp1.getString("mob", null);
        String pwd = sp1.getString("pwd", null);
        String name = sp1.getString("name", null);
        String email = sp1.getString("mail", null);

        if( mob==null|| mob.equals("") || pwd==null|| pwd.equals("")) {
            setContentView(R.layout.activity_login);
        }else{
            Intent loggedin = new Intent(LoginActivity.this, ShopActivity.class);
            startActivity(loggedin);
        }
    }

    public void optionClick(View view){
         if(R.id.signUpBtn == view.getId()){
             findViewById(R.id.namelogin).setVisibility(View.VISIBLE);
             findViewById(R.id.emaillogin).setVisibility(View.VISIBLE);
             findViewById(R.id.phonelogin).setVisibility(View.VISIBLE);
             findViewById(R.id.pwdlogin).setVisibility(View.VISIBLE);
             findViewById(R.id.submitlogin).setVisibility(View.VISIBLE);
         }else{
             findViewById(R.id.namelogin).setVisibility(View.INVISIBLE);
             findViewById(R.id.emaillogin).setVisibility(View.INVISIBLE);
             findViewById(R.id.phonelogin).setVisibility(View.VISIBLE);
             findViewById(R.id.pwdlogin).setVisibility(View.VISIBLE);
             findViewById(R.id.submitlogin).setVisibility(View.VISIBLE);
         }
        findViewById(R.id.signUpBtn).setVisibility(View.INVISIBLE);
        findViewById(R.id.loginBtn).setVisibility(View.INVISIBLE);
    }

    public void saveUserDetails(View view){

        TextView tviewName = (TextView) findViewById(R.id.namelogin);
        TextView tviewEmail = (TextView) findViewById(R.id.emaillogin);
        TextView tviewPhone = (TextView) findViewById(R.id.phonelogin);
        TextView tviewPwd = (TextView) findViewById(R.id.pwdlogin);
        boolean login = false;

        if(findViewById(R.id.namelogin).getVisibility()==View.INVISIBLE && findViewById(R.id.emaillogin).getVisibility()==View.INVISIBLE){
            login = true;
        }

        if(findViewById(R.id.namelogin).getVisibility()==View.VISIBLE &&
                                                    (tviewName.getText().toString()==null|| (tviewName.getText().toString()).equals(""))){
            Toast toastName = Toast.makeText(LoginActivity.this, "Name cannot be empty. Please enter a value", Toast.LENGTH_LONG);
            toastName.show();
            return;
        }

        if(findViewById(R.id.emaillogin).getVisibility()==View.VISIBLE &&
                                                     (tviewEmail.getText().toString()==null|| (tviewEmail.getText().toString()).equals(""))){
            Toast toastEmail = Toast.makeText(LoginActivity.this, "Email cannot be empty. Please enter a value", Toast.LENGTH_LONG);
            toastEmail.show();
            return;
        }

        if(tviewPhone.getText().toString()==null|| (tviewPhone.getText().toString()).equals("")){
            Toast toastPhone = Toast.makeText(LoginActivity.this, "Phone Number cannot be empty. Please enter a value", Toast.LENGTH_LONG);
            toastPhone.show();
            return;
        }

        if(tviewPwd.getText().toString()==null|| (tviewPwd.getText().toString()).equals("")){
            Toast toastPhone = Toast.makeText(LoginActivity.this, "Password Number cannot be empty. Please enter a value", Toast.LENGTH_LONG);
            toastPhone.show();
            return;
        }

        if(!login) {

            try {
                SQLiteOpenHelper raagaDatabaseHelper = new AppDatabaseHelper(LoginActivity.this);
                db = raagaDatabaseHelper.getWritableDatabase();
                ContentValues userValues = new ContentValues();
                userValues.put("FIRST_NAME", tviewName.getText().toString());
                userValues.put("EMAIL", tviewEmail.getText().toString());
                userValues.put("PASSWORD", cryptWithMD5(tviewEmail.getText().toString()));
                userValues.put("MOBILE", Integer.valueOf(tviewPhone.getText().toString()));
                db.insert("USER", null, userValues);
            }catch(SQLiteException e){
                Toast toast = Toast.makeText(LoginActivity.this, "Database unavailable" ,Toast.LENGTH_LONG);
                toast.show();
                e.printStackTrace();
            }

            SharedPreferences sp1 = getSharedPreferences("Login", MODE_PRIVATE);
            sp1 = getSharedPreferences("Login", MODE_PRIVATE);
            SharedPreferences.Editor Ed = sp1.edit();
            Ed.putString("name", tviewName.getText().toString());
            Ed.putString("mail", tviewEmail.getText().toString());
            Ed.putString("mob", tviewPhone.getText().toString());
            Ed.putString("pwd", cryptWithMD5(tviewPwd.getText().toString()));
            Ed.commit();

            Toast toast = Toast.makeText(LoginActivity.this, "Data saved successfully" ,Toast.LENGTH_LONG);
            toast.show();

            Intent loggedin = new Intent(LoginActivity.this, ShopActivity.class);
            startActivity(loggedin);

        }else{
            // password validation

            SharedPreferences sp1 = getSharedPreferences("Login", MODE_PRIVATE);
            sp1 = getSharedPreferences("Login", MODE_PRIVATE);
            SharedPreferences.Editor Ed = sp1.edit();
            Ed.putString("name", "Mark");
            Ed.putString("mail", "mark123@gmail.com");
            Ed.putString("mob", "12345678");
            Ed.putString("pwd", cryptWithMD5("password"));
            Ed.commit();

            Intent loggedin = new Intent(LoginActivity.this, ShopActivity.class);
            startActivity(loggedin);

         }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }



}
