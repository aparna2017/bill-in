package com.example.retail.android2;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by haparn on 7/28/2017.
 */
public class AppDatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "AppTables";
    private static final int DB_VERSION = 1;

    AppDatabaseHelper(Context context){
        super (context,DB_NAME,null,DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        updateMyDatabase(db,0,DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        updateMyDatabase(db,oldVersion,newVersion);
    }

    private void updateMyDatabase (SQLiteDatabase db, int oldVersion, int newVersion){
        if (oldVersion<=1){
            db.execSQL("CREATE TABLE PRODUCTS ("
                    + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "BARCODE TEXT, "
                    + "NAME TEXT, "
                    + "DESC TEXT, "
                    + "COST REAL, "
                    + "STORE_ID INTEGER, "
                    + "FOREIGN KEY (STORE_ID) REFERENCES STORE(_id));");
            db.execSQL("CREATE TABLE TRANSTABLE ("
                    + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "TRANS_NUM TEXT, "
                    + "AMT REAL, "
                    + "STATUS TEXT, "
                    + "TRANS_DATE TEXT, "
                    + "USER_ID INTEGER, "
                    + "STORE_ID INTEGER, "
                    + "FOREIGN KEY (USER_ID) REFERENCES USER(_id), "
                    + "FOREIGN KEY (STORE_ID) REFERENCES STORE(_id));");
            db.execSQL("CREATE TABLE USER ("
                    + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "FIRST_NAME TEXT, "
                    + "LAST_NAME TEXT, "
                    + "EMAIL TEXT, "
                    + "PASSWORD TEXT, "
                    + "MOBILE INTEGER, "
                    + "USER_ID INTEGER, "
                    + "DEFAULT_STORE_ID INTEGER, "
                    + "FOREIGN KEY (USER_ID) REFERENCES USER(_id), "
                    + "FOREIGN KEY (DEFAULT_STORE_ID) REFERENCES STORE(_id));");
            db.execSQL("CREATE TABLE SOLD_PRODUCTS ("
                    + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "SALE_DATE REAL, "
                    + "QUANTITY INTEGER, "
                    + "TRANSACTION_ID INTEGER, "
                    + "PROD_ID INTEGER, "
                    + "USER_ID INTEGER, "
                    + "STORE_ID INTEGER, "
                    + "FOREIGN KEY (USER_ID) REFERENCES USER(_id), "
                    + "FOREIGN KEY (PROD_ID) REFERENCES PRODUCTS(_id), "
                    + "FOREIGN KEY (TRANSACTION_ID) REFERENCES TRANSTABLE(_id), "
                    + "FOREIGN KEY (STORE_ID) REFERENCES STORE(_id));");
            db.execSQL("CREATE TABLE STORE ("
                    + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "BRANCH_CODE TEXT, "
                    + "NAME TEXT, "
                    + "ADDRESS TEXT);");

            insertProduct(db,"PRODUCTS","Hershey's Syrup","Chocolate flaoured syrup" ,"8901071211307" ,6);
            insertProduct(db,"PRODUCTS","Vaseline Deep Restore","Spray Moisturiser" ,"8710908278341" ,21);
            insertProduct(db,"PRODUCTS","Himalaya Pure Hands","Hand sanitiser" ,"8901138819217" ,5);
            insertUser(db,"USER","Mark","mark123@gmail.com" , 123456789);
        }
        if (oldVersion<=2){

        }
    }

    private static void insertProduct(SQLiteDatabase db, String table , String name , String desc , String barcode , int cost){
        ContentValues prodValues = new ContentValues();
        prodValues.put("NAME",name);
        prodValues.put("BARCODE",barcode);
        prodValues.put("DESC",desc);
        prodValues.put("COST",cost);
        db.insert(table,null,prodValues);

    }

    private static void insertUser(SQLiteDatabase db, String table , String name , String email , int num){
        ContentValues prodValues = new ContentValues();
        prodValues.put("FIRST_NAME",name);
        prodValues.put("EMAIL",email);
        prodValues.put("MOBILE",num);
        db.insert(table,null,prodValues);

    }
}
