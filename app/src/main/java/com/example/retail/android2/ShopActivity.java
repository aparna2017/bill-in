package com.example.retail.android2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by haparn on 6/21/2017.
 */
public class ShopActivity extends Activity {

    private ArrayList<String> scannedList;
    private TableLayout table;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
        LocationManager lm = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        if(!gps_enabled && !network_enabled) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);

            dialog.setMessage("Please enable location services to continue");
            dialog.setCancelable(false);
            dialog.setPositiveButton(this.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myGPSIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myGPSIntent);
                    //get gps
                }
            });

            // Cancel option is not provided as location services have to be turned on mandatorily

           /* dialog.setNegativeButton(this.getString(R.string.Cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                }
            });*/

            dialog.show();
        }


    }

    public void storeClicked(View view){
        Intent intent3 = new Intent(ShopActivity.this, MainActivity.class);
        startActivity(intent3);
    }

    public void userClicked(View view){
        Intent intent4 = new Intent(ShopActivity.this, UserActivity.class);
        startActivity(intent4);
    }

    public void settingsClicked(View view){
        Intent intent5 = new Intent(ShopActivity.this, MoreActivity.class);
        startActivity(intent5);
    }

    public void offerClicked(View view){
        Intent intent6 = new Intent(ShopActivity.this, OfferActivity.class);
        startActivity(intent6);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

// OLDER CODE - JUST IN CASE !! - REMOVE BEFORE DEPLOYING

/*   Toast toastchk = Toast.makeText(LoginActivity.this, "yes" , Toast.LENGTH_LONG);
        toastchk.show();   */
// TableRow.LayoutParams trparams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
// trparams.rightMargin = 0; // trparams.width = 300 ; trparams.height = 300 ;
// RelativeLayout.LayoutParams bparams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.WRAPCONTENT);
// bparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//  bparams.height = 500; bparams.width = 500;
//  button.setLayoutParams(bparams);
//  button.setRight(0);
// button.setHeight(20);button.setWidth(20);
//  row.setLayoutParams(trparams);
//item.setTextColor(Color.parseColor("#8BC34A"));
// item.setWidth((int)getResources().getDimension(R.dimen.activity_horizontal_margin)); //  item1.width - @dimen/activity_horizontal_margin
/* adding link to text view and calling onclickListener
CharSequence s1 = "Review your order";
    TextView t1 = (TextView) findViewById(R.id.review);
t1.setText(s1);
        Linkify.addLinks(t1, Linkify.ALL);

        // to save value on focus change of text field
        txtName.setOnFocusChangeListener(new OnFocusChangeListener() {
    public void onFocusChange(View v, boolean hasFocus) {
        if(!hasFocus) {
            saveThisItem(txtClientID.getText().toString(), "name", txtName.getText().toString());
        }
    }
}); */
/*if(getActionBar()!=null) {
            getActionBar().setDisplayShowHomeEnabled(true);
            getActionBar().setLogo(R.mipmap.logo);
            getActionBar().setDisplayUseLogoEnabled(true);
        }

        <!-- <item name="windowActionBarOverlay">true</item>
<item name="android:actionBarStyle">@style/ActionBarTheme</item>
<item name="actionBarStyle">@style/ActionBarTheme</item>--> <!-- not decided yet -->
<!-- Not decided yet -->
   <!-- <style name="ActionBarTheme" parent="Widget.AppCompat.ActionBar">
        <item name="android:background">@color/darkgrey</item>
        <item name="android:icon">@mipmap/logo</item>
        <item name="android:logo">@mipmap/logo</item>
        <item name="android:displayOptions">showHome|useLogo</item>
        <item name="displayOptions">showHome|useLogo</item>
    </style>-->

*/
