package com.example.retail.android2;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by haparn on 6/21/2017.
 */
public class OfferActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ListView listOptions2 = getListView();
        List<String> optionArray = new ArrayList<String>();
        optionArray.add("\n EXCITING DEALS !!! \n ");
        optionArray.add(" Fisrt time users get 15% off on their bill !! Use code 'NEW15' ");

        ArrayAdapter optionsAdapter2 = new ArrayAdapter(this,android.R.layout.simple_list_item_1,optionArray);
        listOptions2.setAdapter(optionsAdapter2);
        listOptions2.setBackgroundResource(R.color.darkgrey);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
