package com.example.retail.android2;

/**
 * Created by haparn on 8/10/2017.
 */
public class Product {

    private int id;
    private String name;
    private String desc;
    private String barcode;
    private Double cost;
    private int storeId;

    public Product( String name, Double cost, int id, String barcode, String desc,  int storeId) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.barcode = barcode;
        this.cost = cost;
        this.storeId = storeId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public String getBarcode() {
        return barcode;
    }

    public Double getCost() {
        return cost;
    }

    public int getStoreId() {
        return storeId;
    }
}
