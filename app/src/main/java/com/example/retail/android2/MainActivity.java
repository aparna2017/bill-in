package com.example.retail.android2;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
// import android.support.v7.app.AppCompatActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import com.example.retail.zxing.IntentIntegrator;
import com.example.retail.zxing.IntentResult;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;


public class MainActivity extends Activity {

    static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
    private ArrayList<Product> productList;
    private ArrayList<String> tempList;
    private TableLayout table;
    private SQLiteDatabase db;
    private Cursor cursor;
    private Double totBillAmt;
    private Button payButton;
    private TextView noOfItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        productList = new ArrayList<Product>();
        setContentView(R.layout.activity_main);
        if(totBillAmt==null||tempList==null){
            totBillAmt = 0.00; tempList= new ArrayList<String>();
        }
        if(savedInstanceState!=null){
            tempList = savedInstanceState.getStringArrayList("tempList");
            totBillAmt = savedInstanceState.getDouble("totBillAmt");
        }

        // Data from Bundle , in the event of an activity pause.
        int j=0;
        for( j=0;j<tempList.size();j++) {
            String s = new String(); s = tempList.get(j);
            String[] productArray = new String[6];productArray = s.split(";");
            Product p = new Product(productArray[0],Double.valueOf(productArray[1]),Integer.valueOf(productArray[2]),productArray[3],productArray[4],Integer.valueOf(productArray[5]));
            productList.add(p);
        }

        // Test Data

        productList.add(new Product("Vaseline Deep Restore - 10",10.00,1,"12345678","ajsdfgagsdfjasgdfj",1));
        productList.add(new Product("Himalaya Pure Hands - 15",15.00,2,"12345678","ajsdfgagsdfjasgdfj",1));

       // Creation of scroll view and table for products

        ScrollView sview = (ScrollView) findViewById(R.id.scrollViewMain);
        table = new TableLayout(this); int tid = 500; // table.setId(tid);
        table.setPadding(0,0,0,0);table.setFitsSystemWindows(true);
        table.setColumnStretchable(0,true);table.setColumnShrinkable(1,true);

        if(sview!=null) {
            sview.addView(table);
        }

        payButton = (Button) findViewById(R.id.button_proceed);
        noOfItems = (TextView)findViewById(R.id.textOne);
        noOfItems.setText(String.valueOf(productList.size()));

        // Code to manually add test data to table
        if(productList.size()==0){
            noOfItems.setVisibility(View.INVISIBLE);
        }
                 int i;
                for (i=0; i<productList.size() ; i++){

                    // Addition of table row for each product
                    TableRow row = new TableRow(this); row.setFitsSystemWindows(true);
                    int idbtn = productList.get(i).getId();
                    row.setId(idbtn+1000);

                    // product description field
                    TextView item = new TextView(this);
                    item.setText(productList.get(i).getName());
                    item.setId(idbtn+3000);
                    item.setPadding(20,35,0,0);
                    item.setSingleLine(false);

                    // delete button for added products
                    ImageButton button = new ImageButton(this);
                    button.setBackgroundColor(View.INVISIBLE);
                    button.setPadding(0,35,20,0);
                    button.setImageResource(R.drawable.deletebutton);
                    button.setId(idbtn);

                    // Addition of item cost to total bill amount
                    TextView itemCost = new TextView(this);itemCost.setText(String.valueOf(productList.get(i).getCost()));
                    itemCost.setVisibility(View.INVISIBLE);
                    itemCost.setId(idbtn+2000);
                    totBillAmt = totBillAmt+productList.get(i).getCost();
                    if(totBillAmt!=null&totBillAmt!=0) {
                        payButton.setText(" Pay (" + totBillAmt + " AED) ");
                    }

                    // Listener for delete button. Product will be removed from screen on click of this butto.
                    button.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            int clickedId = v.getId();
                            Double minusCost = Double.valueOf(((TextView)findViewById(clickedId + 2000)).getText().toString());
                            totBillAmt=totBillAmt-minusCost;
                            table.removeView(findViewById(clickedId+1000));
                            if(totBillAmt!=0) {
                                payButton.setText(" Pay (" + totBillAmt + " AED) ");
                            }else{
                                payButton.setText(" Pay ");
                            }
                            for (Product p : productList){
                                if (p.getId()==clickedId)
                                productList.remove(p);
                            }
                            if(productList.size()!=0) {
                                noOfItems.setText(String.valueOf(productList.size()));
                            }else{
                                noOfItems.setVisibility(View.INVISIBLE);
                            }
                        }
                    });
                    row.addView(item);  row.addView(button);row.addView(itemCost);row.setPadding(0,0,5,10);
                    table.addView(row);
                }
    }

    // Bar Code scanner call

    public void scanClicked(View view){
        try {
            IntentIntegrator scanIntegrator = new IntentIntegrator(this);
            scanIntegrator.initiateScan();
            }
        catch (ActivityNotFoundException anfe) {
            CharSequence msg = "No Scanner found";
            Toast toast = Toast.makeText(this,msg,Toast.LENGTH_LONG);
            toast.show();
        }
    }


    // Bar Code scanner return call

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult != null) {
            String scanContent = scanningResult.getContents();
            Toast toast1 = Toast.makeText(MainActivity.this, scanContent, Toast.LENGTH_LONG);
            toast1.show();
            // Code to add actual scanned products to table
            if (scanContent != null) {
                String scanFormat = scanningResult.getFormatName();
                TextView item = new TextView(this);
                TextView itemCost = new TextView(this);
                try {
                    SQLiteOpenHelper appDatabaseHelper = new AppDatabaseHelper(this);
                    db = appDatabaseHelper.getReadableDatabase();
                    cursor = db.query("PRODUCTS", new String[]{"_id", "NAME", "BARCODE", "DESC", "COST", "STORE_ID"},
                            "BARCODE=?", new String[]{scanContent}, null, null, null);
                    if (cursor.moveToFirst()) {
                        Toast toast2 = Toast.makeText(MainActivity.this, cursor.getString(1), Toast.LENGTH_LONG);
                        toast2.show();
                        String nameVal = cursor.getString(1);
                        Double cost = cursor.getDouble(4);
                        if(totBillAmt==null){
                            totBillAmt = 0.00;
                        }
                        totBillAmt = totBillAmt + cost;
                        Product p = new Product(nameVal,cost,cursor.getInt(0),cursor.getString(2),cursor.getString(3),cursor.getInt(5));
                        productList.add(p);
                        tempList.add(nameVal+";"+cost+";"+cursor.getString(0)+";"+cursor.getString(2)+";"+cursor.getString(3)+";"+cursor.getString(5));
                        item.setText(nameVal);
                        itemCost.setText(String.valueOf(cost));
                    }
                } catch (SQLiteException e) {
                    Toast toast = Toast.makeText(MainActivity.this, "Database unavailable", Toast.LENGTH_LONG);
                    e.printStackTrace();
                    toast.show();
                }

                noOfItems = (TextView) findViewById(R.id.textOne);
                noOfItems.setText(String.valueOf(productList.size()));
                noOfItems.setVisibility(View.VISIBLE);
                payButton = (Button) findViewById(R.id.button_proceed);
                if(totBillAmt!=null&totBillAmt!=0) {
                    payButton.setText(" Pay (" + totBillAmt + " AED) ");
                }
                TableRow row = new TableRow(this);
                int idbtn = productList.get(0).getId();
                row.setId(idbtn + 1000);

                item.setPadding(20,35,0,0);
                item.setSingleLine(false);
                item.setId(idbtn+3000);

                ImageButton button = new ImageButton(this);
                button.setBackgroundColor(View.INVISIBLE);
                button.setImageResource(R.drawable.deletebutton);
                button.setPadding(0,35,20,0);
                button.setId(idbtn);

                itemCost.setId(idbtn+2000);
                itemCost.setVisibility(View.INVISIBLE);

                button.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        int clickedId = v.getId();
                        Double minusCost = Double.valueOf(((TextView)findViewById(clickedId + 2000)).getText().toString());
                        totBillAmt=totBillAmt-minusCost;
                        table.removeView(findViewById(clickedId + 1000));
                        if(totBillAmt!=0) {
                            payButton.setText(" Pay (" + totBillAmt + " AED) ");
                        }else{
                            payButton.setText(" Pay ");
                        }
                        for (Product p : productList){
                            if (p.getId()==clickedId)
                                productList.remove(p);
                        }
                        if(productList.size()!=0) {
                            noOfItems.setText(String.valueOf(productList.size()));
                        }else{
                            noOfItems.setVisibility(View.INVISIBLE);
                        }

                    }
                });

                row.addView(item);row.addView(itemCost);row.addView(button);row.setPadding(0, 0, 5, 10);
                table.addView(row);

            }
        }
    }

    // Call to payment page

    public void proceedClicked(View view){
        Intent intent1 = new Intent(MainActivity.this, PaymentActivity.class);
        intent1.putStringArrayListExtra("scannedProducts",tempList);
        startActivity(intent1);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putDouble("totBillAmt",totBillAmt);
        savedInstanceState.putStringArrayList("productList",tempList);
    }

}
